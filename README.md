# Currículo - Henrique Hideki Kuwai

> Simple Vue.js resume for studying.

I created this resumeé using Vue.JS for studying. Feel free to clone the repo and make your changes - if you want, you can use this project to build your own resume!

---

## For DEVS - Build Setup

``` bash
# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

Built with [vue-cli](https://github.com/vuejs/vue-cli).
